<!DOCTYPE html>
<html>
<head>
    <title>Form Tambah Data</title>
</head>
<body>
<script>
    fetch('http://localhost/minapi/api.php')
        .then(res =>{
            return res.json();
        })
        .then(data => {
            console.log(data);
            data.forEach(user =>{
                const markup = `<li>${user.nama}</li>`;

                document.querySelector('ul').insertAdjacentHTML('beforeend',markup);
            })
        })
</script>

<a href="index.php">Home</a>
<h2>Form Tambah Data</h2>
<ul></ul>
<!-- <form method="post" action="add.php">
    <label for="nama">Nama:</label><br>
    <input type="text" id="nama" name="nama" required><br><br>

    <label for="email">Email:</label><br>
    <input type="email" id="email" name="email" required><br><br>

    <label for="mobile">Mobile:</label><br>
    <input type="text" id="mobile" name="mobile" required><br><br>

    <input type="submit" value="Add">
</form> -->
<form method="post" action="add.php">
    <label for="nama">Nama:</label><br>
    <input type="text" id="nama" name="nama" required oninput="fillData(this.value)"><br><br>

    <label for="email">Email:</label><br>
    <input type="email" id="email" name="email" required readonly><br><br>

    <label for="mobile">Mobile:</label><br>
    <input type="text" id="mobile" name="mobile" required readonly><br><br>

    <input type="submit" value="Add">
</form>

<script>
    function fillData(nama) {
        fetch('http://localhost/minapi/api.php')
            .then(res => res.json())
            .then(data => {
                const user = data.find(user => user.nama === nama);
                if (user) {
                    document.getElementById('email').value = user.email;
                    document.getElementById('mobile').value = user.mobile;
                }
            })
            .catch(error => console.log(error));
    }
</script>



</body>
</html>
