<!DOCTYPE html>
<html>
<head>
    <title>Edit Data Tamu</title>
</head>
<body>

<?php
include "config.php";

// Cek apakah parameter 'id' telah dikirimkan melalui URL
if (isset($_GET['id'])) {
    $id = $_GET['id'];

    // Query untuk mendapatkan data tamu berdasarkan ID
    $sql = "SELECT * FROM tamu2 WHERE ID = $id";
    $result = $conn->query($sql);

    if ($result->num_rows == 1) {
        $row = $result->fetch_assoc();
        $nama = $row['name'];
        $email = $row['email'];
        $mobile = $row['message'];

        // Form untuk mengedit data tamu
        echo "<h2>Edit Data Tamu</h2>";
        echo "<form method='post' action='proses_edit_data.php'>";
        echo "<input type='hidden' name='id' value='$id'>";
        echo "Nama: <input type='text' name='nama' value='$nama' required><br><br>";
        echo "Email: <input type='email' name='email' value='$email' required><br><br>";
        echo "Mobile: <input type='text' name='mobile' value='$mobile' required><br><br>";
        echo "<input type='submit' value='Update'>";
        echo "</form>";
    } else {
        echo "Data tidak ditemukan.";
    }
} else {
    echo "ID tidak tersedia.";
}

$conn->close();
?>

</body>
</html>
