<?php
// Mengecek apakah metode HTTP adalah POST
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Menerima nilai dari form login
    $username = $_POST["username"];
    $password = $_POST["password"];

    // Cek username dan password
    if ($username === "admin" && $password === "admin123") {
        // Jika username dan password valid, redirect ke halaman utama
        header("Location: index.php");
        exit;
    } else {
        // Jika username dan password tidak valid, tampilkan pesan error
        $error = "Username atau password salah.";
    }
}
?>

<!DOCTYPE html>
<html>
<head>
    <title>Login</title>
</head>
<body>

<h2>Login</h2>

<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
    <label for="username">Username:</label><br>
    <input type="text" id="username" name="username" required><br><br>

    <label for="password">Password:</label><br>
    <input type="password" id="password" name="password" required><br><br>

    <input type="submit" value="Login">
</form>

<?php if (isset($error)) { ?>
    <p><?php echo $error; ?></p>
<?php } ?>

</body>
</html>
