<!DOCTYPE html>
<html>
<head>
    <title>Data Tamu</title>
</head>
<body>
<a href="tambah_data.php">Tambah User</a>
<h2>Data Tamu</h2>

<table border="1">
    <tr>
        <th>Nama</th>
        <th>Mobile</th>
        <th>Email</th>
        <th>Action</th>
    </tr>

    <?php
    // Koneksi ke database
    include "config.php";

    // Mengambil data dari tabel tamu2
    $sql = "SELECT * FROM tamu2";
    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        // Menampilkan data dalam bentuk tabel
        while ($row = $result->fetch_assoc()) {
            echo "<tr>";
            echo "<td>" . $row["name"] . "</td>";
            echo "<td>" . $row["message"] . "</td>";
            echo "<td>" . $row["email"] . "</td>";
            echo "<td><a href='edit.php?id=" . $row["id"] . "'>Edit</a> | <a href='delete.php?id=" . $row["id"] . "'>Delete</a></td>";
            echo "</tr>";
        }
    } else {
        echo "<tr><td colspan='4'>Tidak ada data tamu.</td></tr>";
    }

    $conn->close();
    ?>
</table>

</body>
</html>
