<!DOCTYPE html>
<html>
<head>
    <title>Data Mahasiswa</title>
</head>
<body>
    <h2>Data Mahasiswa</h2>
    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $nim = $_POST['nim'];
        $nama = $_POST['nama'];
        $alamat = $_POST['alamat'];
        $nilai_harian = $_POST['nilai_harian'];
        $nilai_midtest = $_POST['nilai_midtest'];
        $nilai_finaltest = $_POST['nilai_finaltest'];

        // Menghitung Total Nilai
        $total_nilai = $nilai_harian + $nilai_midtest + $nilai_finaltest;

        // Menghitung Nilai Rata-rata
        $rata_rata = $total_nilai / 3;

        // Format angka dengan 2 desimal di belakang koma
        $total_nilai = number_format($total_nilai, 2);
        $rata_rata = number_format($rata_rata, 2);

        // Menentukan Grade
        if ($rata_rata >= 89) {
            $grade = "A";
        } elseif ($rata_rata >= 79) {
            $grade = "B";
        } elseif ($rata_rata >= 69) {
            $grade = "C";
        } elseif ($rata_rata >= 59) {
            $grade = "D";
        } else {
            $grade = "E";
        }

        // Menentukan Predikat
        switch ($grade) {
            case "A":
                $predikat = "Amat Baik";
                break;
            case "B":
                $predikat = "Baik";
                break;
            case "C":
                $predikat = "Cukup";
                break;
            case "D":
                $predikat = "Kurang";
                break;
            default:
                $predikat = "Gagal";
        }

        // Menampilkan data
        echo "<p>NIM: " . $nim . "</p>";
        echo "<p>Nama: " . $nama . "</p>";
        echo "<p>Alamat: " . $alamat . "</p>";
        echo "<p>Nilai Harian: " . $nilai_harian . "</p>";
        echo "<p>Nilai Mid Test: " . $nilai_midtest . "</p>";
        echo "<p>Nilai Final Test: " . $nilai_finaltest . "</p>";
        echo "<p>Total Nilai: " . $total_nilai . "</p>";
        echo "<p>Nilai Rata-rata: " . $rata_rata . "</p>";
        echo "<p>Grade: " . $grade . "</p>";
        echo "<p>Predikat: " . $predikat . "</p>";
    }
    ?>
</body>
</html>
