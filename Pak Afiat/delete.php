<?php
// Mengecek apakah parameter 'id' telah dikirimkan melalui URL
if (isset($_GET['id'])) {
    // Mengambil nilai ID dari URL
    $id = $_GET['id'];

    // Koneksi ke database
    include "config.php";

    // Query untuk menghapus data tamu berdasarkan ID
    $sql = "DELETE FROM tamu2 WHERE ID = $id";

    if ($conn->query($sql) === TRUE) {
        echo "Data berhasil dihapus.";
    } else {
        echo "Error: " . $sql . "<br>" . $conn->error;
    }

    $conn->close();
} else {
    echo "ID tidak tersedia.";
}
?>
