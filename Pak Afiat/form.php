<!DOCTYPE html>
<html>
<head>
    <title>Form Processing</title>
</head>
<body>
    <h2>Form Input Data Mahasiswa</h2>
    <form action="process.php" method="POST">
        <label for="nim">NIM:</label>
        <input type="text" id="nim" name="nim" required><br><br>

        <label for="nama">Nama:</label>
        <input type="text" id="nama" name="nama" maxlength="20" required><br><br>

        <label for="alamat">Alamat:</label>
        <input type="text" id="alamat" name="alamat" maxlength="20" required><br><br>

        <label for="nilai_harian">Nilai Harian:</label>
        <input type="number" id="nilai_harian" name="nilai_harian" min="0" max="100" required><br><br>

        <label for="nilai_midtest">Nilai Mid Test:</label>
        <input type="number" id="nilai_midtest" name="nilai_midtest" min="0" max="100" required><br><br>

        <label for="nilai_finaltest">Nilai Final Test:</label>
        <input type="number" id="nilai_finaltest" name="nilai_finaltest" min="0" max="100" required><br><br>

        <input type="submit" value="Process">
    </form>
</body>
</html>
